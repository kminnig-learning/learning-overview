# Learning Overview

## Purpose

This repo is used to track my tech learning and share anything that might be useful.

My goal is to use free tooling as much as possible.

## Topics

### DevOps

#### GitLab (TBD)

### Languages

#### Python (TBD)

#### GO (TBD)

### Observability

#### Grafana Labs (TBD)

#### DataDog (TBD)

### Conferences (TBD)

#### DockerCon 2023 (TBD)

#### Kubecon 2023 (TBD)





## License
Licenced under [AGPL](LICENSE)
